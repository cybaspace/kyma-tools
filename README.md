# Developer Tools for KYMA

Kyma is an open source project to extend appications with serverless functions and microservices:  
https://kyma-project.io

This collection of tools is designed to make it easier for developers to work with Kyma.

The tool collection can be used by clone this repository and call the bash scripts.  
Alternatively the corresponding docker image (registry.gitlab.com/cybaspace/kyma-tools) can be called, e.g.  
`docker run --rm registry.gitlab.com/cybaspace/kyma-tools version`
 
## Create Certificate for external Application
An external application which wants to communicate with a kyma installation (kyma runtime hosted by SAP or own hosted) must use a certificate signed by kyma in the communication.

The creation of a certificate is an annoying process which can be automated by a script:  
`generate-certificate.sh`

Also you can use the existing docker image with the script:  
`docker run --rm -v ~/kubeconfig.yml:/.kube/config -v ~/kyma-app-certs:/opt/cert -e APPLICATION_NAME=sample-store maihiro/kyma-cert`
